<div class="relative mt-3 md:mt-0" x-data="{ isOpen: true }" @click.away="isOpen = false">
    <!-- using LiveWire and tailwind Css snipper and alpainJs [x.open] -->
    <input
        wire:model.debounce.100ms="search"
        type="text" class="bg-gray-800 text-sm rounded-full w-64 px-4 pl-8 py-1 focus:outline-none focus:shadow-outline"
        placeholder="Search"
        @focus="isOpen = true"
        @keydown="isOpen = true"
        @keydown.escape.window="isOpen = false"
    >
    <div wire:loading class="spinner top-0 right-0 mr-4 mt-3"></div>
    @if(strlen($search ) >= 2 )
        <div class="z-50 absolute bg-gray-800 text-sm rounded w-64 mt-4"  x-show.transition.opacity="isOpen">
            @if($searchResults->count() > 0)
                <ul>
                    @foreach($searchResults as $search)
                        <li class="border-b border-gray-700">
                            <a href="{{route('movies.show',$search['id'])}}" class="block hover:bg-gray-700 px-3 py-3 flex items-center transition ease-in-out duration-150">
                                @if ($search['poster_path'])
                                    <img src=" https://image.tmdb.org/t/p/w92/{{ $search['poster_path'] }}" alt="poster" class="w-8">
                                @else
                                    <img src=" https://via.placeholder.com/50x75" alt="poster" class="w-8">
                                @endif
                                <span class="ml-4">{{ $search['title'] }}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            @else
                <div class="px-3 py-3" >
                    No Result for "{{$search}}"
                </div>
            @endif
        </div>
    @endif
</div>
