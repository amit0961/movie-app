<?php

namespace App\ViewModels;

use Spatie\ViewModels\ViewModel;

class MovieViewModel extends ViewModel
{
    public $movieDetails;

    public function __construct($movieDetails)
    {
        $this->movieDetails = $movieDetails;
    }


    public function movie()
    {
        return $this->movieDetails;
    }
}
